
public class AusgabeformatierungBeispiele {

	public static void main(String[] args) {
		
		System.out.printf("|%20s|%n", "123456789");
		System.out.printf("|%-20s|%n", "123456789");
		System.out.printf("|%-20.3s|", "123456789");

		System.out.println("");
		System.out.println("");
/* Kommentar 
 * 
 *  	
 */
	
		System.out.printf("|%-20d|%n",123456789);
		System.out.printf("|%+-20d|",123456789);
		
		System.out.println("");
		System.out.println("");System.out.println("");
		System.out.println("");
		
		System.out.printf("%f%n", 12345.567889);
		System.out.printf("%.2f%n", 12345.567889);
		System.out.printf("|%20.2f|%n", 12345.567889);
		System.out.printf("|%-20.2f|", 12345.567889);	
		
		
		System.out.printf("Der Preis von %s ist %.2f. Euro%n", "Monitor", 109.95);
	
		
	}

}