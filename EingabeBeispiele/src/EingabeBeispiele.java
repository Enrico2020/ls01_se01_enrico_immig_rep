import java.util.Scanner;

public class EingabeBeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner=new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl ein:");
		int zahl1=myScanner.nextInt();
		System.out.println("zahl1: "+zahl1);
		System.out.println("Geben Sie bitte Ihren Vornamen ein:");
		String vorname =myScanner.next();
		System.out.println("Vorname: "+vorname);
		System.out.println("Geben Sie bitte ein Symbole ein: ");
		char buchstabe=myScanner.next().charAt(0);
		System.out.println("Buchstabe "+ buchstabe);

	}

}