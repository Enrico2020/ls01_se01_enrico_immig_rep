import java.util.Scanner;

class Fahrkartenautomat_Methoden
{
    public static void main(String[] args)
    {
      
    //Methodenaufrufe
    	
       //Fahrkartenpreis
    	
    	double zuZahlenderBetrag=fahrkartenbestellungErfassen();
      
       // Geldeinwurf und R�ckgeldberechnung
           	
    	double r�ckgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       
    	fahrkartenAusgeben();

       //R�ckgeldausgabe
       
    	rueckgeldAusgeben(r�ckgabebetrag);
       
    }
    
    //Methoden   
   
        
    public static double fahrkartenbestellungErfassen() {
		Scanner myScanner= new Scanner(System.in);
		System.out.println("Zu zahlender Betrag (EURO): ");
		double bestellung = myScanner.nextDouble();
	return bestellung;
	
	}
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner myScanner= new Scanner(System.in);
    	double eingezahlterGesamtbetrag=0.0;
    	double differenz;
    	double eingeworfeneM�nze;
	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    	{
    		differenz=(zuZahlenderBetrag-eingezahlterGesamtbetrag);   
    		System.out.printf("Noch zu zahlen: %.2f Euro.%n", differenz);
    		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    		eingeworfeneM�nze = myScanner.nextDouble();
    		eingezahlterGesamtbetrag += eingeworfeneM�nze;
    	}
        double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	return r�ckgabebetrag;
        
    }	
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");		
    	    	
    }
    
   
    public static void rueckgeldAusgeben(double rueckgeld) {
    	
            if(rueckgeld > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro%n",rueckgeld);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt: ");

            while(rueckgeld >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
         	 rueckgeld -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
     }	
    	
}