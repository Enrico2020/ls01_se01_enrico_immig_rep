import java.util.Scanner;

public class PCHaendler {
	
	//Hauptprogramm
	
	public static void main(String[] args) {

		double Mehrwertsteuersatz, Nettopreis, Nettogesamtpreis, Bruttogesamtpreis;
		String bestellung;
		int Anzahl;
		
		// Benutzereingaben lesen		
		
			bestellung=liesString("Was m�chten Sie bestellen?");
				
			Anzahl=liesInt("Geben Sie die Anzahl ein:");
			
			Mehrwertsteuersatz=liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
			
			Nettopreis=liesDouble("Geben Sie den Nettopreis ein:");
				
		// Verarbeiten
			
			Nettogesamtpreis=berechneGesamtnettopreis(Anzahl,Nettopreis);	
			Bruttogesamtpreis=berechneGesamtbruttopreis(Nettogesamtpreis, Mehrwertsteuersatz);

		// Ausgeben

			rechungausgeben(bestellung,Anzahl,Nettogesamtpreis,Bruttogesamtpreis,Mehrwertsteuersatz);
		
	}
	
	//Unterprogramme/Methoden/Funktionen
	
		//Benutzereingaben lesen

	public static String liesString(String text) {
		Scanner myScanner= new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
	return artikel;
	}
	
	
	public static int liesInt(String text) {
		Scanner myScanner= new Scanner(System.in);
		System.out.println(text);
	    int zahl = myScanner.nextInt();
	return zahl;
	}
	
	public static double liesDouble(String text) {
		Scanner myScanner= new Scanner(System.in);
		System.out.println(text);
		double wert = myScanner.nextDouble();	
	return wert;
	
	}
	
	// Verarbeiten
		
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
	double nettogesamtpreis = anzahl * nettopreis;
	return nettogesamtpreis;
	}
	
		
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
	double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
	return bruttogesamtpreis;
	}
	
	// Ausgeben
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");	
	
	}
}
	