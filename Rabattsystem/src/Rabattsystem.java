import java.util.Scanner;

public class Rabattsystem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Eingabe
		double Bestellwert = BestellwertEinlesen();
		// Verarbeitung
		double BestellwertmitRabatt = Rabattberechen(Bestellwert);
		double Nettopreis = MehrwertsteuerAddieren(BestellwertmitRabatt);
		// Ausgabe
		RechnungsbetragAusgeben(Nettopreis);
	}

	public static double BestellwertEinlesen() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bestellwert eingeben");
		double Eingabe = myScanner.nextDouble();
		return Eingabe;
	}

	public static double Rabattberechen(double Bestellwertparameter) {

		double Rabatt;
		Rabatt = 0;
		if ((Bestellwertparameter > 0) && (Bestellwertparameter < 100)) {
			Rabatt = 0.1 * Bestellwertparameter;
		} else if ((Bestellwertparameter >= 100) && (Bestellwertparameter < 500)) {
			Rabatt = 0.15 * Bestellwertparameter;
		} else if (Bestellwertparameter >= 500) {
			Rabatt = 0.20 * Bestellwertparameter;
		}
		else
			System.err.println("Bitte geben sie einen korrekten Bestellwert ein!");
		return Bestellwertparameter - Rabatt;

	}

	public static double MehrwertsteuerAddieren(double Bruttopreis) {
		double Steuer = Bruttopreis * 0.19;
		return Bruttopreis + Steuer;
	}

	public static void RechnungsbetragAusgeben(double Ausgabe) {
		System.out.printf("Der erm��igte Bestellwert inkl. Mehrwertsteuer betr�gt: %.2f Euro%n", Ausgabe);
	}
}
