/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >> 
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = (long)2E9;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = (int)3769E3; 
     
    // Wie alt bist du?  Wie viele Tage sind das?
    
     int alterTage = 12389; 
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
     int gewichtKilogramm = (int)150E3;   
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
     double  flaecheGroessteLand = 17.1E6;
    
    // Wie groß ist das kleinste Land der Erde?
    
     float flaecheKleinsteLand = 0.44f;
     
     
    
         
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Berlin hat " + bewohnerBerlin + " Einwohner.");  
    
    System.out.println("Ich bin " + alterTage + "Tage alt.");   
    
    System.out.println("Der Blauwahl wiegt " + gewichtKilogramm + " KG.");
    
    System.out.println("Die Fl�che Ru�lands betr�gt " + flaecheGroessteLand + " Quadratkilometer.");   
    
    System.out.println("Die Fl�che der Vatikanstadt betr�gt " + flaecheKleinsteLand + " Quadratkilometer.");   
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

