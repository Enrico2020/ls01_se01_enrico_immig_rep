﻿/* Operatoren.java
   Uebung zu Operatoren in Java
   @author
   @version
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/

  
    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");

    float  zahl1, zahl2;
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */

    zahl1=75;
    zahl2=23;
    System.out.println("Zahl 1 = " + zahl1);
    System.out.println("Zahl 2 = " + zahl2);
    
    
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

    float zahl3;
    zahl3=zahl1+zahl2;
    System.out.println("Ergebnis von " + zahl1 + " + " + zahl2 + " = " + zahl3);

    
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */

    zahl3=zahl1-zahl2;
    System.out.println("Ergebnis von " + zahl1 + " - " + zahl2 + " = " + zahl3);

    
    zahl3=zahl1*zahl2;
    System.out.println("Ergebnis von " + zahl1 + " * " + zahl2 + " = " + zahl3);

    float zahl4;
    zahl4=zahl1/zahl2;
    System.out.println("Ergebnis von " + zahl1 + " / " + zahl2 + " = " + zahl4);

    		
    		
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    
    boolean vergleich;
    
    vergleich=zahl1==zahl2;
    System.out.println("Ergebnis von " + zahl1 + " = " + zahl2 + " ist " + vergleich);
    

    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
    
    vergleich=zahl1!=zahl2;
    System.out.println("Ergebnis von " + zahl1 + " ungleich " + zahl2 + " ist " + vergleich);
    
    vergleich=zahl1<zahl2;
    System.out.println("Ergebnis von " + zahl1 + " < " + zahl2 + " ist " + vergleich);
    
    vergleich=zahl1>zahl2;
    System.out.println("Ergebnis von " + zahl1 + " > " + zahl2 + " ist " + vergleich);
    
    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    
    if ((0 <= zahl1 && zahl1 <= 50) && (0 <= zahl2 && zahl2 <= 50))
    System.out.println("0<= " + zahl1 + " <= 50" + "0<= " + zahl2 + " <= 50");
    else
    System.out.println("Die Werte liegen nicht beide im Wertebereich");     
    	
  }//main
}// Operatoren
